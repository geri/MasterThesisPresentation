\def\fileversion{1.0}
\def\filedate{09/02/2022}%dd/mm/yyyy

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{presentationTemplate}[\filedate\space Version \fileversion\space by
  Riccardo Maria Gesuè]

\LoadClass[12pt,aspectratio=169]{beamer}

%% Fonts %%
\usepackage{amsmath,amssymb,txfonts}
% \usepackage[sfdefault]{noto}
% \usepackage[default]{cantarell}
\usepackage{libertinus}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

%% Refs %%
% \usepackage[colorlinks=true]{hyperref}

%% Captions %%
\usepackage[font=small,textfont=it,labelfont=bf,format=hang]{caption}
\usepackage{subcaption}

%% Geometry %%
\usepackage{geometry}

%% Tables %%
\usepackage{booktabs}
\usepackage{multirow}

%% Slide layout %%
\usepackage{multicol}

%% Figures %%
% \usepackage{graphicx}

\usepackage{fancyhdr}
\usepackage{setspace}

\usepackage{tikz}
\usetikzlibrary{calc}

%% Theme %%
% \usetheme{Madrid}
% \useinnertheme{rectangles}
% \useoutertheme{}

%% TOC %%
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{1}
\setbeamertemplate{sections/subsections in toc}[square]
\AtBeginSection[]
{
  \begin{frame}{Outline}
  \tableofcontents[
    sectionstyle=show/shaded,
    subsectionstyle=hide/hide/hide
  ]
  \end{frame}
}



%% Formatting %%
% \setlength\parskip{5 px}

%% navigation symbols %%
\setbeamertemplate{navigation symbols}{}
% \setbeamertemplate{footline}[frame number]

%% Appendix %%
\usepackage{appendixnumberbeamer}
\AtBeginDocument{
	\apptocmd{\appendix}{
		\pgfkeys{
			/metropolis/outer/.cd,
			numbering=none,
			progressbar=none}
		}{}{}
	}

%% Slide styling %%
\input{colorscheme/colors.sty}
\input{colorscheme/masterThesis.sty}
